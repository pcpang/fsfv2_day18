/**
 * Created by pohchye on 22/7/2016.
 */
(function () {
    angular.module("ShopApp")
        .service("shopService", shopService);

    shopService.$inject = ["$http", "$q"];

    function shopService($http, $q) {
        var shop = this;

        shop.addToCart = function (item) {
            var defer = $q.defer();
            console.log("Item: " + item);
            $http.post("/api/cart/add", item)
                .then(function (results) {
                    console.log("addToCart Service result: ", results.data);
                    return defer.resolve(results.data);
                })
                .catch(function (err) {
                    return defer.reject(err);
                });
            return defer.promise;
        };

        shop.refreshCart = function () {
            var defer = $q.defer();

            $http.get("/api/cart/refresh", {params: {
                userId: userId
            }})
                .then(function(results) {
                    console.log("refreshCart service result: " + result.data);
                    return defer.resolve(results.data)
                })
                .catch(function(err) {
                    return defer.reject(err)
                });

            return defer.promise;
        };

        shop.checkout = function () {
            var defer = $q.defer();
            $http.get("/api/cart/checkout")
                .then(function (results) {
                    console.log("checkout Service result: " + results.data);
                    return defer.resolve(results.data);
                })
                .catch(function (err) {
                    return defer.reject(err);
                });
            return defer.promise;
        };

        shop.itemObject = function () {
            var item = {};
            item.name = "";
            item.quantity = "";
            return item;
        };
    }
    
})();