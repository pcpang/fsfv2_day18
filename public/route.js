/**
 * Created by pohchye on 22/7/2016.
 */
(function () {

    angular
        .module("ShopApp")
        .config(ShopConfig);

    function ShopConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("clear", {
                url: "/clear",
                templateUrl: "/views/clear.html",
                controller: "ShopCtrl as ctrl"
            })
            .state("details", {
                url: "/details",
                templateUrl: "/views/details.html",
                controller: "ShopCtrl as ctrl"
            });

        $urlRouterProvider.otherwise("/clear");
    }

    ShopConfig.$inject = ["$stateProvider", "$urlRouterProvider"];


})();