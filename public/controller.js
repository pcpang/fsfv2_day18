/**
 * Created by pohchye on 22/7/2016.
 */
(function () {
    angular.module("ShopApp")
        // .controller("DetailCtrl", DetailCtrl)
        .controller("ShopCtrl", ShopCtrl);

    function ShopCtrl(shopService) {
        // var vm = this;
        // vm.newItem = shopService.itemObject();
        // vm.cart = [];

        var vm = this;

        vm.newItem = {};
        vm.cart = [];
        vm.msg = "";

        vm.addToCart = function () {
            shopService.addToCart(vm.newItem)
                .then(function () {
                    vm.msg = "Item was added to cart successfully";
                    // vm.newItem = items;
                })
                .catch(function (err) {
                    // console.log("Somer Error Occured", err);
                    vm.msg = "There was some problem adding item to cart"
                });
        };

        vm.refreshCart = function () {
            console.log(vm.newItem.userId);
            shopService.refreshCart(vm.newItem.userId)
                .then(function (items) {
                    vm.cart = items;
                })
                .catch(function (err) {
                    // console.log("Somer Error Occured", err);
                    vm.msg = "Error fetching your cart!";
                });
        };

        vm.checkout = function () {
            shopService.checkout()
                .then(function () {
                    vm.msg = "Checked out cart!";
                })
                .catch(function (err) {
                    // console.log("Somer Error Occured", err);
                    vm.msg = "Error fetching your cart!"
                });
        };
    }

    ShopCtrl.$inject = ["shopService"];
    
    
})();