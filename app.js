/**
 * Created by pohchye on 22/7/2016.
 */

var express = require("express");
var app=express();

//Load express

var bodyParser=require("body-parser");
// app.use(bodyParser.urlencoded({'extended': 'true'}));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// var queue = require("q");
var q = require("q");

function generateRandomId() {
    return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 15);
}

// var cookieParser = require("cookie-parser");
// app.use(cookieParser(), function (req, res, next) {
//     if (!req.cookies.userId) {
//         res.cookie("userId", generateRandomId());
//     }
//     next();
// });

var cart = {};

var session = require("express-session");
var MySQLStore = require("express-mysql-session")(session);
var mysqlstore = new MySQLStore({
    host: "localhost",
    user: "root",
    password: "fsfv2",
    database: "mysql_session"
});
app.use(session({
    secret: "lt7P0tx4t4",
    resave: false,
    saveUninitialized: true,
    store: mysqlstore
}));
app.use(function(req, res, next) {
    if(! req.session.cart ) {
        req.session.cart = [];
    }
    next()
});

// Add Cart
app.post("/api/cart/add", function (req, res) {
    // console.info("TODO: This method will implement the add to cart feature");
    // var userId = req.body.userId;
    // console.log("Cart for userid before push", userId, cart[userId]);
    // cart[userId] = cart[userId] || [];
    // cart[userId].push({
    //     name: req.body.name,
    //     quantity: req.body.quantity
    // });
    // console.info("Cart for userid after push", userId, cart[userId]);
    // res.status(202).end();
    req.session.cart.push({                // Push new value in array
        name: req.body.name,
        quantity: req.body.quantity
    });
    // console.log("Session: ", req.session);
    console.log("Cart:", req.session.cart);
    res.status(202).end();
});

// Refresh Cart
app.get("/api/cart/refresh", function(req, res) {
    // console.log("Sending back cart information");
    // var userId = req.query.userId;
    // res.json(cart[userId]);
    console.log("Sending back cart information");
    res.json(req.session.cart);
});

// Checkout
app.post("/api/cart/checkout", function(req, res) {
    // console.log("Cart has been checked out");
    // var userId = req.body.userId;
    // cart[userId] = [];
    // res.status(202).end();
    console.log("Cart has been checked out");
    req.session.cart = [];
    res.status(202).end();
});

// Serve public files
app.use(express.static(__dirname + "/public"));
// Handle Errors
app.use(function (req, res, http) {
    res.redirect("error.html");
});

// Set port
app.set("port",
    process.argv[2] || process.env.APP_PORT || 3018
);

// Start the Express server
app.listen(app.get("port"), function () {
    console.info("Express server started on port %s", app.get("port"));
});
